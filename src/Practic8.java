import oracle.jdbc.rowset.OracleCachedRowSet;
import oracle.jdbc.rowset.OracleJDBCRowSet;

import java.io.*;
import java.sql.*;

public class Practic8 {

    public static void main(String[] args) {
        System.out.println("Connecting to the database ...");
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";
        String file = "practic8.crs";
        registerDriver("oracle.jdbc.OracleDriver");
        try {
            // Execute statement and save results to selected file
            try (Connection conn = DriverManager.getConnection(jdbcUrl, user, pass);
                 Statement st = conn.createStatement();
                 OracleCachedRowSet crs = new OracleCachedRowSet();
                 ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {

                ResultSet rs = st.executeQuery("SELECT АББРЕВИАТУРА, НАИМЕНОВАНИЕ FROM Н_ЦИКЛЫ_ДИСЦИПЛИН");
                crs.populate(rs);
                out.writeObject(crs);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
