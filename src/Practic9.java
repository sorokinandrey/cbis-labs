import oracle.jdbc.rowset.OracleCachedRowSet;
import oracle.jdbc.rowset.OracleJDBCRowSet;

import javax.sql.RowSet;
import java.io.*;
import java.sql.*;

public class Practic9 {

    public static void main(String[] args) {
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";
        String file = "practic8.crs";
        String table = "s183328Practic9";
        registerDriver("oracle.jdbc.OracleDriver");
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            OracleCachedRowSet crs = (OracleCachedRowSet) in.readObject();
            prettyPrintResult(crs);
            try (Connection conn = DriverManager.getConnection(jdbcUrl, user, pass)) {
                createNewTableFromRowSet(conn, crs, table);
                crs.setCommand("SELECT * FROM " + table);
                crs.execute(conn);
                clear(conn, table);
                prettyPrintResult(crs);
            }

        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void prettyPrintResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            String name = rsmd.getColumnName(i);
            System.out.format("%-25s", name + " (" + rsmd.getColumnTypeName(i) + ")");
        }
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.format("%-25s", rs.getString(i));
            }
            System.out.println();
        }
    }

    private static void createNewTableFromRowSet(Connection conn, RowSet rs, String table) {
        try (Statement st = conn.createStatement()) {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();

            StringBuilder createQuery = new StringBuilder("CREATE TABLE " + table + " (");
            StringBuilder insertQuery = new StringBuilder("INSERT INTO " + table + " VALUES(");

            clear(conn, table);
            for (int i = 1; i <= columnCount; i++) {
                createQuery.append(rsmd.getColumnName(i) + " " + rsmd.getColumnTypeName(i));
                createQuery.append("(" + rsmd.getColumnDisplaySize(i) + "), ");
                insertQuery.append("?, ");
            }
            if (createQuery.length() > 1) {
                createQuery.setCharAt(createQuery.length() - 2, ')');
                createQuery.deleteCharAt(createQuery.length() - 1);
                insertQuery.setCharAt(insertQuery.length() - 2, ')');
                insertQuery.deleteCharAt(insertQuery.length() - 1);
            }


            System.out.println(createQuery.toString());
            st.executeQuery(createQuery.toString());
            System.out.println(insertQuery.toString());

            PreparedStatement insertSt = conn.prepareStatement(insertQuery.toString());
            rs.beforeFirst();
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    insertSt.setObject(i, rs.getObject(i));
                }
                insertSt.addBatch();
            }
            insertSt.executeBatch();
            insertSt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private static void clear(Connection conn, String table) {
        String dropQuery = "DROP TABLE " + table;
        System.out.println(dropQuery);
        try (Statement st = conn.createStatement()){
            st.executeQuery(dropQuery);
        } catch (SQLException ignored) {

        }
    }
}
