import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class Practic1 {

    public static void main(String[] args) {
        System.out.println("Connecting to the database ...");
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";
        registerDriver("oracle.jdbc.OracleDriver");
//        try (Connection conn = getConnection(jdbcUrl, user, pass)) {
        try (Connection conn = getConnection(new FileInputStream("ad.database.properties"))) {
            System.out.println("Запрос 1.1:");
            executeStatement(conn, "SELECT * FROM Н_ЦИКЛЫ_ДИСЦИПЛИН", Practic1::prettyPrintResult);
            System.out.println("Запрос 1.2:");
            executeStatement(conn, "SELECT АББРЕВИАТУРА, НАИМЕНОВАНИЕ FROM Н_ЦИКЛЫ_ДИСЦИПЛИН", Practic1::prettyPrintResult);
            System.out.println("Запрос 1.3:");
            executeStatement(conn, "SELECT ИД, НАИМЕНОВАНИЕ FROM Н_КВАЛИФИКАЦИИ", Practic1::prettyPrintResult);
            System.out.println("Запрос 2.1:");
            executeStatement(conn, "SELECT DISTINCT ИМЯ FROM Н_ЛЮДИ WHERE LENGTH(TRIM(ИМЯ)) > 0", Practic1::prettyPrintResult);
            System.out.println("Запрос 2.2:");
            executeStatement(conn, "SELECT DISTINCT ПРИЗНАК FROM Н_УЧЕНИКИ", Practic1::prettyPrintResult);
        } catch (SQLException | FileNotFoundException se) {
            se.printStackTrace();
        }
    }

    private static void registerDriver(Driver driver) {
        try {
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection(String url, String user, String pass) throws SQLException {
        return DriverManager.getConnection(url, user, pass);
    }

    private static Connection getConnection(InputStream in) throws SQLException {
        Properties props = new Properties();
        try {
            try {
                props.load(in);
                props.list(System.out);
                String drivers = props.getProperty("drivers");
                if (drivers != null) {
                    System.setProperty("jdbc.drivers", drivers);
                    System.out.println("Property \"jdbc.drivers\" = " +
                            System.getProperty("jdbc.drivers"));
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = props.getProperty("url");
        return DriverManager.getConnection(url, props);
    }

    interface Printable<T> {
        void print(T rs) throws SQLException;
    }

    static void executeStatement(final Connection conn, final String sql, Printable<ResultSet> callback) throws SQLException {
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        callback.print(rs);
        st.close();
    }

    private static void prettyPrintResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            String name = rsmd.getColumnName(i);
            System.out.format("%-25s", name);
        }
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.format("%-25s", rs.getString(i));
            }
            System.out.println();
        }
    }
}