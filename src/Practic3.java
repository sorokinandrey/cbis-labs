import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class Practic3 {

    public static void main(String[] args) {
        System.out.println("Connecting to the database ...");
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";
        registerDriver("oracle.jdbc.OracleDriver");
        try (Connection conn = getConnection(jdbcUrl, user, pass)) {
            System.out.println("Задание 1:");
            executeStatement(conn, "SELECT * FROM Н_ЦИКЛЫ_ДИСЦИПЛИН", (st, rs) -> {
                System.out.println("Row number " + rs.getRow() + "; BFR is " + rs.isBeforeFirst());
                while (rs.next()) {
                    System.out.print("Row number " + rs.getRow() + "; First is " + rs.isFirst());
                    System.out.print(":\t" + rs.getInt(1));
                    System.out.println("\t" + rs.getString(3));
                    System.out.println("Row number "+rs.getRow()+"; Last is "+rs.isLast());
                }
                System.out.println("Row number " + rs.getRow() + "; ALR is " + rs.isAfterLast());
            }, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            System.out.println("Задание 2:");
            executeStatement(conn, "SELECT * FROM Н_ЦИКЛЫ_ДИСЦИПЛИН", (st, rs) -> {
                rs.afterLast();
                System.out.println("Row number " + rs.getRow() + "; BFR is " + rs.isBeforeFirst());
                rs.last();
                while (rs.previous()) {
                    System.out.print("Row number " + rs.getRow() + "; First is " + rs.isFirst());
                    System.out.print(":\t" + rs.getInt(1));
                    System.out.println("\t" + rs.getString(3));
                    System.out.println("Row number " + rs.getRow() + "; Last is " + rs.isLast());
                }
                System.out.println("Row number " + rs.getRow() + "; ALR is " + rs.isAfterLast());
            }, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

            System.out.println("Задание 3:");
            prepareDatabase(conn);
            executeStatement(conn, "SELECT id, title FROM TEST183328", (st, rs) -> {
                while (rs.next()) {
                    System.out.println(rs.getInt(1)+"\t"+rs.getString(2));
                    if(rs.getString(2).equals("МТС")) {
                        rs.updateString(2, "TELE2");
                        rs.updateRow();
                    }
                }
//                rs = st.executeQuery("SELECT * FROM операторы_связи");
                rs.beforeFirst();
                while (rs.next()) {
                    System.out.println(rs.getInt(1)+"\t"+rs.getString(2));
                }
            }, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            System.out.println("Задание 4:");
            prepareDatabase(conn);
            executeStatement(conn, "SELECT id, title FROM TEST183328", (st, rs) -> {
                while (rs.next()) {
                    System.out.println(rs.getInt(1) + "\t" + rs.getString(2));
                    if (rs.getString(2).equals("SkyLink")) {
                        rs.deleteRow();
                    }
                }
                rs.moveToInsertRow();
                rs.updateInt(1, 6);
                rs.updateString(2, "TELE2");
                rs.insertRow();
                rs = st.executeQuery("SELECT * FROM TEST183328");
                rs.beforeFirst();
                while (rs.next()) {
                    System.out.println(rs.getInt(1) + "\t" + rs.getString(2));
                }
            }, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void prepareDatabase(Connection conn) {
        try {
            executeStatement(conn, "DROP TABLE TEST183328", (st, rs) -> {
            });
        } catch (SQLException e) {
            // Ignore
        }
        try (Statement st = conn.createStatement()) {
            st.addBatch("CREATE TABLE TEST183328(id NUMBER(4) PRIMARY KEY, title VARCHAR(10))");
            st.addBatch("INSERT INTO TEST183328 VALUES(1,'Мегафон')");
            st.addBatch("INSERT INTO TEST183328 VALUES(2,'МТС')");
            st.addBatch("INSERT INTO TEST183328 VALUES(3,'БиЛайн')");
            st.addBatch("INSERT INTO TEST183328 VALUES(4,'SkyLink')");
            st.addBatch("INSERT INTO TEST183328 VALUES(5,'Yota')");
            st.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection(String url, String user, String pass) throws SQLException {
        return DriverManager.getConnection(url, user, pass);
    }


    interface Printable<T1, T2> {
        void print(T1 st, T2 rs) throws SQLException;
    }

    static void executeStatement(final Connection conn, final String sql, Printable<Statement, ResultSet> callback) throws SQLException {
        executeStatement(conn, sql, callback, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    }

    static void executeStatement(final Connection conn, final String sql, Printable<Statement, ResultSet> callback, int resultSetType, int resultSetConcurrency) throws SQLException {
        Statement st = conn.createStatement(resultSetType, resultSetConcurrency);
        ResultSet rs = st.executeQuery(sql);
        callback.print(st, rs);
        st.close();
    }
}