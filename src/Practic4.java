import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Practic4 {

    public static void main(String[] args) {
        System.out.println("Connecting to the database ...");
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";

        try (Connection conn = DriverManager.getConnection(jdbcUrl, user, pass);
             Statement st = conn.createStatement()) {
            try {
                st.executeQuery("DROP TABLE TEST183328");
            } catch (SQLException e) {
                // Ignore
            }
            st.executeQuery("CREATE TABLE TEST183328(id NUMBER(4) PRIMARY KEY, title VARCHAR(50), price NUMBER(6), is_sale NUMBER(1))");
            PreparedStatement prst = conn.prepareStatement("INSERT INTO TEST183328 VALUES(?, ?, ?, ?)");
            prst.setInt(1, 1);
            prst.setString(2, "OnePlus 5");
            prst.setInt(3, 494);
            prst.setInt(4, 0);
            prst.execute();
            prst.setInt(1, 2);
            prst.setString(2, "iPhone 7 Plus");
            prst.setInt(3, 700);
            prst.setInt(4, 1);
            prst.execute();
            prst.setInt(1, 3);
            prst.setString(2, "Samsung Galaxy S8");
            prst.setInt(3, 664);
            prst.setInt(4, 1);
            prst.execute();
            prst.setInt(1, 4);
            prst.setString(2, "Samsung Galaxy S8 Plus");
            prst.setInt(3, 733);
            prst.setInt(4, 0);
            prst.execute();
            prst.setInt(1, 5);
            prst.setString(2, "LG G6");
            prst.setInt(3, 449);
            prst.setInt(4, 1);
            prst.execute();
            prst.setInt(1, 6);
            prst.setString(2, "HTC U11");
            prst.setInt(3, 649);
            prst.setInt(4, 1);
            prst.execute();
            prst.close();
            prettyPrintResult(st.executeQuery("SELECT * FROM TEST183328"));
            prettyPrintResult(conn.getMetaData().getSchemas());

            try {
                st.executeQuery("DROP TABLE TEST183328");
            } catch (SQLException e) {
                // Ignore
            }
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    private static void prettyPrintResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            String name = rsmd.getColumnName(i);
            System.out.format("%-25s", name);
        }
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.format("%-25s", rs.getString(i));
            }
            System.out.println();
        }
    }
}