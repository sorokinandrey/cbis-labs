//Шаг первый: Импорт пакетов

import java.io.*;
import java.sql.*;

public class Practic2 {

    public static void createTables(Statement st) throws SQLException {
        String operatorsql = "CREATE TABLE операторы_связи "
                + "(ид NUMBER(3) CONSTRAINT ПК_ОС PRIMARY KEY,"
                + "оператор VARCHAR(20))";
        String telefonsql = "CREATE TABLE номера_телефонов "
                + "(ид_л NUMBER(9) CONSTRAINT ВК_Л REFERENCES н_люди(ид),"
                + "номер_телефона VARCHAR(20),"
                + "дата_регистрации DATE,"
                + "ид_ос NUMBER(3) CONSTRAINT ВК_ОС REFERENCES операторы_связи(ид))";
        try {
            st.executeUpdate("DROP TABLE операторы_связи CASCADE CONSTRAINTS");
        } catch (SQLException se) {
            //Игнорировать ошибку удаления таблицы
            if (se.getErrorCode() == 942) {
                String msg = se.getMessage();
                System.out.println("Ошибка при удалении таблицы: " + msg);
            }
        }

        //Создание таблицы операторы_связи
        if (st.executeUpdate(operatorsql) == 0)
            System.out.println("Таблица операторы_связи создана...");
        try {
            st.executeUpdate("DROP TABLE номера_телефонов");
        } catch (SQLException se) {
            //Игнорировать ошибку удаления таблицы
            if (se.getErrorCode() == 942) {
                String msg = se.getMessage();
                System.out.println("Ошибка при удалении таблицы: " + msg);
            }
        }

        //Создание таблицы номера_телефонов
        if (st.executeUpdate(telefonsql) == 0)
            System.out.println("Таблица номера_телефонов создана...");
    }

/*	public static void insertData(Statement st) throws SQLException {
        //Загрузка данных в таблицу операторы_связи
		st.executeUpdate("INSERT INTO операторы_связи VALUES(1,'Мегафон')");
		st.executeUpdate("INSERT INTO операторы_связи VALUES(2,'МТС')");
		st.executeUpdate("INSERT INTO операторы_связи VALUES(3,'Би Лайн')");
		st.executeUpdate("INSERT INTO операторы_связи VALUES(4,'SkyLink')");

		//Загрузка данных в таблицу номера_телефонов
		st.executeUpdate("INSERT INTO номера_телефонов VALUES(125704,'9363636',{d '2000-9-15'},1)");
		st.executeUpdate("INSERT INTO номера_телефонов VALUES(125704,'2313131',{d '2001-2-25'},2)");
		st.executeUpdate("INSERT INTO номера_телефонов VALUES(125704,'1151515',{d '2002-6-17'},4)");
		st.executeUpdate("INSERT INTO номера_телефонов VALUES(120848,'4454545',{d '2003-7-30'},3)");
		st.executeUpdate("INSERT INTO номера_телефонов VALUES(120848,'1161616',{d '2004-1-16'},4)");

		System.out.println("Загрузка данных закончена...");
	}*/

    public static void batchInsert(Statement st) throws SQLException {
        //Загрузка данных в таблицу операторы_связи
        st.addBatch("INSERT INTO операторы_связи VALUES(1,'Мегафон')");
        st.addBatch("INSERT INTO операторы_связи VALUES(2,'МТС')");
        st.addBatch("INSERT INTO операторы_связи VALUES(3,'Би Лайн')");
        st.addBatch("INSERT INTO операторы_связи VALUES(4,'SkyLink')");
        //Загрузка данных в таблицу номера_телефонов
        st.addBatch("INSERT INTO номера_телефонов VALUES(125704,'9363636',{d '2000-9-15'},1)");
        st.addBatch("INSERT INTO номера_телефонов VALUES(125704,'2313131',{d '2001-2-25'},2)");
        st.addBatch("INSERT INTO номера_телефонов VALUES(125704,'1151515',{d '2002-6-17'},4)");
        st.addBatch("INSERT INTO номера_телефонов VALUES(120848,'4454545',{d '2003-7-30'},3)");
        st.addBatch("INSERT INTO номера_телефонов VALUES(120848,'1161616',{d '2004-1-16'},4)");
        System.out.println("Пакет загрузки данных подготовлен...");
        //st.clearBatch();
        try {
            int[] count = st.executeBatch();
            for (int i = 0; i < count.length; i++) System.out.print(count[i] + "\t");
            st.clearBatch();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        st.clearBatch();
    }

    public static void rollbackInsert(Connection conn) throws SQLException {
        Savepoint svpt = null;
        try {
            conn.setAutoCommit(false);
            if (!conn.getAutoCommit()) System.out.println("Auto-Commit отменен...");
            Statement st = conn.createStatement();
            String sql = "INSERT INTO операторы_связи VALUES(5,'TELE2')";
            st.executeUpdate(sql);
            //
            svpt = conn.setSavepoint("Point1");
            //
            sql = "INSERT INTO номера_телефонов VALUES(130777,'2223322',{d '2004-2-3'},5)";
            st.executeUpdate(sql);
            conn.commit();
        } catch (SQLException se) {
            System.out.println("SQLException сообщение: " + se.getMessage());
            System.out.println("Начнем откат изменений...");
            try {
                //conn.rollback();
                conn.rollback(svpt);
            } catch (SQLException rse) {
                rse.printStackTrace();
            }
            System.out.println("Откат изменений закончен...");

        }
        conn.setAutoCommit(true);
    }

    public static void insertData(Connection conn) throws SQLException {
        //Загрузка данных в таблицу операторы_связи
        int count;
        String sql = "INSERT INTO операторы_связи VALUES(?,?)";
        PreparedStatement prst = conn.prepareStatement(sql);
        //
        prst.setInt(1, 1);
        prst.setString(2, "Мегафон");
        count = prst.executeUpdate();
        if (count == 1)
            System.out.println("Запись \"Мегафон\" добавлена");
        prst.setInt(1, 2);
        prst.setString(2, "МТС");
        count = prst.executeUpdate();
        if (count == 1)
            System.out.println("Запись \"МТС\" добавлена");
        prst.setInt(1, 3);
        prst.setString(2, "Би Лайн");
        count = prst.executeUpdate();
        if (count == 1)
            System.out.println("Запись \"Би Лайн\" добавлена");
        prst.setInt(1, 4);
        prst.setString(2, "SkyLink");
        count = prst.executeUpdate();
        if (count == 1)
            System.out.println("Запись \"SkyLink\" добавлена");
        prst.close();
        //
        //Загрузка данных в таблицу номера_телефонов
        sql = "INSERT INTO номера_телефонов VALUES(?,?,?,?)";
        prst = conn.prepareStatement(sql);
        conn.setAutoCommit(false);
        //
        prst.setInt(1, 125704);
        prst.setString(2, "9363636");
        prst.setDate(3, Date.valueOf("2000-9-15"));
        prst.setInt(4, 1);
        prst.addBatch();
        prst.setInt(1, 125704);
        prst.setString(2, "2313131");
        prst.setDate(3, Date.valueOf("2001-2-25"));
        prst.setInt(4, 2);
        prst.addBatch();
        prst.setInt(1, 125704);
        prst.setString(2, "1151515");
        prst.setDate(3, Date.valueOf("2002-6-17"));
        prst.setInt(4, 4);
        prst.addBatch();
        prst.setInt(1, 120848);
        prst.setString(2, "4454545");
        prst.setDate(3, Date.valueOf("2003-7-30"));
        prst.setInt(4, 3);
        prst.addBatch();
        prst.setInt(1, 120848);
        prst.setString(2, "1161616");
        prst.setDate(3, Date.valueOf("2004-1-16"));
        prst.setInt(4, 4);
        prst.addBatch();
        int[] countb = prst.executeBatch();
        for (int i = 0; i < countb.length; i++) System.out.print(countb[i] + "\t");
        prst.clearBatch();
        prst.close();
        conn.commit();
        conn.setAutoCommit(true);
        System.out.println("\n" + "Загрузка данных закончена...");
    }

    public static void createReadmeTable(Statement st) throws SQLException {
        String sql = "CREATE TABLE readme (id INTEGER, data LONG)";
        try {
            st.executeUpdate("DROP TABLE readme");
        } catch (SQLException se) {
            //Игнорировать ошибку удаления таблицы
            if (se.getErrorCode() == 942) {
                String msg = se.getMessage();
                System.out.println("Ошибка при удалении таблицы: " + msg);
            }
        }
        //Создание таблицы
        if (st.executeUpdate(sql) == 0)
            System.out.println("Таблица readme создана...");
    }

    public static void streamInsert(Connection conn) throws SQLException, IOException {
        Statement st = null;
        PreparedStatement prst = null;
        ResultSet rs = null;

        st = conn.createStatement();
        createReadmeTable(st);
        //
        File f = new File("/etc/ssh/sshd_config");
        long fileLength = f.length();
        FileInputStream fis = new FileInputStream(f);
        //
        String sql = "INSERT INTO readme VALUES(?,?)";
        prst = conn.prepareStatement(sql);
        prst.setInt(1, 1);
        prst.setAsciiStream(2, fis, (int) fileLength);
        prst.execute();
        //
        fis.close();
        //
        sql = "SELECT * FROM readme WHERE id = 1";
        rs = st.executeQuery(sql);
        if (rs.next()) {
            InputStream is = rs.getAsciiStream(2);
            int c;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((c = is.read()) != -1) baos.write(c);
            System.out.println(baos.toString());
        }
        st.close();
        prst.close();
    }

    public static void createProc(Connection conn) throws SQLException {
        Statement st = conn.createStatement();
        String sql = "CREATE PROCEDURE get_fio "
                + "(id IN NUMBER, fio OUT VARCHAR) AS "
                + "BEGIN "
                + "SELECT человек(id, 'И', 9) INTO fio FROM DUAL; "
                + "END;";
        try {
            st.executeUpdate("DROP PROCEDURE get_fio");
        } catch (SQLException se) {
            //Игнорировать ошибку удаления процедуры
            if (se.getErrorCode() == 4043) {
                String msg = se.getMessage();
                System.out.println("Ошибка при удалении процедуры: " + msg);
            }
        }
        //Создание процедуры
        if (st.executeUpdate(sql) == 0)
            System.out.println("Процедура get_fio создана...");
    }

    public static void executeProc(Connection conn) throws SQLException {
        String sql = "{call get_fio(?,?)}";
        CallableStatement cst = conn.prepareCall(sql);
        cst.setInt(1, 121018);
        cst.registerOutParameter(2, Types.VARCHAR);
        cst.execute();
        System.out.println("Результат запроса: " + cst.getString(2));
    }


    public static void main(String[] args) {

        Connection conn = null;

        try {

            //Шаг второй: Регистрация JDBC драйвера
            String driver = "oracle.jdbc.OracleDriver";
            Class.forName(driver);

            //Шаг третий: Создание соединения
            System.out.println("Connecting to database ...");
            String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
            String user = "s158320";
            String pass = "qpr472";
            conn = DriverManager.getConnection(jdbcUrl, user, pass);

            //Шаг четвертый: Выполнение запроса
            Statement st = conn.createStatement();
            createTables(st);
            batchInsert(st);
            rollbackInsert(conn);
            createTables(st);
            st.close();
            insertData(conn);
            streamInsert(conn);
            createProc(conn);
            executeProc(conn);
            conn.close();

            //Шаг седьмой: Обработка исключений
        } catch (SQLException se) {
            //Обработка ошибок для JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Обработка ошибок для Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resourses
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try

        System.out.println("GoodBye!");
    }//end main
}//end FirstQuery
