import oracle.jdbc.rowset.OracleCachedRowSet;
import oracle.jdbc.rowset.OracleJDBCRowSet;

import java.io.*;
import java.sql.*;

public class Practic7 {

    public static void main(String[] args) {
        System.out.println("Connecting to the database ...");
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";
        String file = "practic7.crs";
        registerDriver("oracle.jdbc.OracleDriver");
        try {
            // Execute statement and save results to selected file
            try (OracleCachedRowSet crs = new OracleCachedRowSet();
                 ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
                crs.setUrl(jdbcUrl);
                crs.setUsername(user);
                crs.setPassword(pass);

                //Set and execute the command. Notice the parameter query.
                String sql = "SELECT s.ГРУППА, p.ФАМИЛИЯ, p.ИМЯ, p.ОТЧЕСТВО, p.ДАТА_РОЖДЕНИЯ, p.МЕСТО_РОЖДЕНИЯ FROM Н_УЧЕНИКИ s " +
                        "LEFT OUTER JOIN Н_ЛЮДИ p ON p.ИД = s.ЧЛВК_ИД " +
                        "WHERE s.ГРУППА = ?";
                crs.setCommand(sql);
                crs.setInt(1, 4102);
                crs.execute();

                //Serialize CachedRowSet object.;
                out.writeObject(crs);
            }
            // Read and print results
            try (FileInputStream fis = new FileInputStream(file);
                 ObjectInputStream in = new ObjectInputStream(fis)) {
                OracleCachedRowSet crs = (OracleCachedRowSet) in.readObject();
                prettyPrintResult(crs);
            }
        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void prettyPrintResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            String name = rsmd.getColumnName(i);
            System.out.format("%-25s", name);
        }
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.format("%-25s", rs.getString(i));
            }
            System.out.println();
        }
    }
}
