import oracle.jdbc.rowset.OracleCachedRowSet;
import oracle.jdbc.rowset.OracleJDBCRowSet;

import java.io.*;
import java.sql.*;
import java.util.Properties;

public class Practic6 {

    public static void main(String[] args) {
        System.out.println("Connecting to the database ...");
        String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:orbis";
        String user = "s183328";
        String pass = "bow331";
        String file = "cachedrs.crs";
        registerDriver("oracle.jdbc.OracleDriver");
        try {
            System.out.println("Пункт 1:");
            OracleJDBCRowSet ojrs = new OracleJDBCRowSet();
            ojrs.setUrl(jdbcUrl);
            ojrs.setUsername(user);
            ojrs.setPassword(pass);
            ojrs.setCommand("SELECT count(*) FROM н_люди");
            ojrs.execute();
            prettyPrintResult(ojrs);

            System.out.println("Пункт 2:");
            OracleCachedRowSet ocrs = new OracleCachedRowSet();
            ocrs.setUrl(jdbcUrl);
            ocrs.setUsername(user);
            ocrs.setPassword(pass);
            ocrs.setCommand("SELECT * FROM н_люди WHERE ид = ?");
            ocrs.setInt(1, 142530);
            ocrs.execute();
            prettyPrintResult(ocrs);

            System.out.println("Пункт 3:");
            writeCachedRowSet(jdbcUrl, user, pass, file);
            OracleCachedRowSet crs = readCachedRowSet(file);
            prettyPrintResult(crs);
            crs.close();

            System.out.println("Пункт 4:");
            try (Connection conn = DriverManager.getConnection(jdbcUrl, user, pass);
                 Statement st = conn.createStatement()) {
                Practic2.createTables(st);
                Practic2.batchInsert(st);
            }
            crs = new OracleCachedRowSet();
            crs.setUrl(jdbcUrl);
            crs.setUsername(user);
            crs.setPassword(pass);
            String sql = "SELECT * FROM номера_телефонов WHERE ид_л = ?";
            crs.setCommand(sql);
            crs.setInt(1, 125704);
            crs.execute();
            crs.setReadOnly(false);
            prettyPrintResult(crs);

            System.out.println("1 обновлен:");
            crs.first();
            crs.updateString(2, "1131313");
            crs.updateRow();
            crs.acceptChanges();
            prettyPrintResult(crs);

            System.out.println("Вставлена строка:");
            crs.moveToInsertRow();
            crs.updateInt(1, 120848);
            crs.updateString(2, "4454545");
            crs.updateDate(3, Date.valueOf("2017-10-30"));
            crs.updateInt(4, 1);
            crs.insertRow();
            crs.acceptChanges();
            prettyPrintResult(crs);

            System.out.println("Отмена обновления 1:");
            crs.first();
            crs.updateString(2, "235345");
            crs.updateRow();
            crs.cancelRowUpdates();
            prettyPrintResult(crs);

            System.out.println("Отмена обновления нескольких:");
            crs.moveToInsertRow();
            crs.updateInt(1, 111111);
            crs.updateString(2, "wrong");
            crs.updateDate(3, Date.valueOf("2017-12-01"));
            crs.updateInt(4, 0);
            crs.insertRow();
            crs.moveToInsertRow();
            crs.updateInt(1, 222222);
            crs.updateString(2, "wrong");
            crs.updateDate(3, Date.valueOf("2017-11-01"));
            crs.updateInt(4, 1);
            crs.insertRow();
            crs.restoreOriginal();
            prettyPrintResult(crs);
            crs.close();
        } catch (SQLException | IOException | ClassNotFoundException se) {
            se.printStackTrace();
        }
    }

    private static void registerDriver(String driver) {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void writeCachedRowSet(String url, String user, String pass, String file) throws SQLException, IOException {
        //Instantiate a CachedRowSet object, set connection parameters
        OracleCachedRowSet crs = new OracleCachedRowSet();
        crs.setUrl(url);
        crs.setUsername(user);
        crs.setPassword(pass);

        //Set and execute the command. Notice the parameter query.
        String sql = "SELECT * FROM н_люди WHERE ид = ?";
        crs.setCommand(sql);
        crs.setInt(1, 142531);
        crs.execute();

        //Serialize CachedRowSet object.
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        out.writeObject(crs);
        out.close();
        crs.close();
    }//end writeCachedRowSet()

    public static OracleCachedRowSet readCachedRowSet(String file) throws IOException, ClassNotFoundException {
        //Read serialized CachedRowSet object from storage
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream in = new ObjectInputStream(fis);
        OracleCachedRowSet crs = (OracleCachedRowSet) in.readObject();
        fis.close();
        in.close();
        return crs;
    }//end readCachedRowSet()

    private static void prettyPrintResult(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            String name = rsmd.getColumnName(i);
            System.out.format("%-25s", name);
        }
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.format("%-25s", rs.getString(i));
            }
            System.out.println();
        }
    }
}
